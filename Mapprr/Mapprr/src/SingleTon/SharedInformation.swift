//
//  SharedInformation.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import Foundation

class SharedInformation {
    
    private static var sharedInformation : SharedInformation? = nil
    var repositoriesResponse : RepositoriesModels? = nil
    var contributorsResponse : [ContributorsModels]? = nil
    var contributorsRepoListResponse : [ContributorsRepoListModels]? = nil
    
    var serachBarText : String? = nil
    var contributorsUrl: String? = nil
    var contributorsRepoListUrl: String? = nil

    static func instance() -> SharedInformation {
        if (sharedInformation == nil) {
            sharedInformation = SharedInformation()
        }
        return sharedInformation!
    }
    
    private init() {
        // Fetch logged in keys
    }
}
