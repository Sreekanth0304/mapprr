//
//  GetRepositoriesServiceWebAPI.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import Foundation

class GetRepositoriesServiceWebAPI {
    
    private static var getRepositoriesServiceWebAPI : GetRepositoriesServiceWebAPI? = nil
    static let repositoriesServiceMethodName = "search/repositories?q="
    
    static func instance() -> GetRepositoriesServiceWebAPI {
        if (getRepositoriesServiceWebAPI == nil) {
            getRepositoriesServiceWebAPI = GetRepositoriesServiceWebAPI()
        }
        return getRepositoriesServiceWebAPI!
    }
    
    public func getRepositoriesServiceDetails(_ completionHandler: @escaping(RepositoriesModels?) -> Void) -> URLSessionDataTask {

        let urlBuilder = APIInterface.baseURL + GetRepositoriesServiceWebAPI.repositoriesServiceMethodName + SharedInformation.instance().serachBarText!
        print(urlBuilder)
        var request = URLRequest(url: URL(string: urlBuilder)!)
        request.httpMethod = "GET"
        
        return APIInterface.instance().executeAuthenticatedRequest(request: request, completionHandler: { (data, response, error) in
            if let data = data {
                do {
                    // You have to first convert response data into String and then convert that Sting to Data using UTF8 encoding before decoding.
                    let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8)
                    let jsonDecoder = JSONDecoder()
                    let loadResponseModel = try jsonDecoder.decode(RepositoriesModels.self, from: utf8Data!)
                    completionHandler(loadResponseModel)
                    let httpResponse = response as? HTTPURLResponse
                    if httpResponse!.statusCode.description == "400" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Bad Request")
                    }else if httpResponse!.statusCode.description == "401" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Session got expired please login again")
                    }else if httpResponse!.statusCode.description == "503" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Server is not available")
                    }
                }
                catch let error as NSError {
                    APIInterface.instance().showError(error: error)
                    completionHandler(nil)
                }
            }
        })
    }
}
