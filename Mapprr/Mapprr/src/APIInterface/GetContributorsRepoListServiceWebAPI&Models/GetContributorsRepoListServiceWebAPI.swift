//
//  GetContributorsRepoListServiceWebAPI.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import Foundation

class GetContributorsRepoListServiceWebAPI {
    
    private static var getContributorsRepoListServiceWebAPI : GetContributorsRepoListServiceWebAPI? = nil
    
    static func instance() -> GetContributorsRepoListServiceWebAPI {
        if (getContributorsRepoListServiceWebAPI == nil) {
            getContributorsRepoListServiceWebAPI = GetContributorsRepoListServiceWebAPI()
        }
        return getContributorsRepoListServiceWebAPI!
    }
    
    public func getContributorsRepoListServiceDetails(_ completionHandler: @escaping([ContributorsRepoListModels]?) -> Void) -> URLSessionDataTask {

        
        let urlBuilder = SharedInformation.instance().contributorsRepoListUrl
        if urlBuilder == nil {
            APIInterface.instance().showAlert(title: "", message: "This contributor does not have repo list")
        }
        print(urlBuilder as Any)
        var request = URLRequest(url: URL(string: urlBuilder!)!)
        request.httpMethod = "GET"
        
        return APIInterface.instance().executeAuthenticatedRequest(request: request, completionHandler: { (data, response, error) in
            if let data = data {
                do {
                    // You have to first convert response data into String and then convert that Sting to Data using UTF8 encoding before decoding.
                    let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8)
                    let jsonDecoder = JSONDecoder()
                    let loadResponseModel = try jsonDecoder.decode([ContributorsRepoListModels].self, from: utf8Data!)
                    completionHandler(loadResponseModel)
                    let httpResponse = response as? HTTPURLResponse
                    if httpResponse!.statusCode.description == "400" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Bad Request")
                    }else if httpResponse!.statusCode.description == "401" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Session got expired please login again")
                    }else if httpResponse!.statusCode.description == "503" {
                        APIInterface.instance().showAlert(title: httpResponse!.statusCode.description, message: "Server is not available")
                    }
                }
                catch let error as NSError {
                    APIInterface.instance().showError(error: error)
                    completionHandler(nil)
                }
            }
        })
    }
}
