//
//  ContributorsDetailsCollectionViewCell.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 24/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit

class ContributorsDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
}
