//
//  ViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {

    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isSeacrhActive = false
    var searchController : UISearchController!
    let transitionViewController = TransitionViewController()
    var repositoriesItemsArray = [RepositoriesItems]()
    var sortedRepositoriesItemsArray = [RepositoriesItems]()
    var tenRowsArray : [RepositoriesItems] = [RepositoriesItems]()
    var filteredDateSlectedData : [RepositoriesItems] = [RepositoriesItems]()
    
    var selectedDateString = String()
    var checkDate = false
    var defalutImage = UIImage(named: "Empty-Image.png")
    var busSearchResultsWithReturnMutableArray : NSMutableArray! = NSMutableArray();
    var repoArray = [RepositoriesItems]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // Transition Delegete
        navigationController?.delegate = self
        
        tableview.isHidden = true
        hideActivityIndicator()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tableview.separatorStyle = .none
        tableview.estimatedRowHeight = 140
        tableview.rowHeight = UITableView.automaticDimension
    }

    @IBAction func filtterButtonTapped(_ sender: Any) {
        
        if tableview.isHidden == true {
            APIInterface.instance().showAlert(title: "It seems like tableview data not shown in TableView.", message: "Please first search by Repositories and add filter")
        } else {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController
            vc?.filterDateDelegate = self
            vc?.viewController = self
            navigationController?.pushViewController(vc!, animated: true)
        }
    }
}

extension ViewController {
    
    // Get data from API
    func getRepositoriesServiccall() {
        
        showActivityIndicator()
        
        // step1: check all validations
        guard (searchBar.text?.count != 0) else {
                APIInterface.instance().showAlert(title: "", message: "Please search by repository")
                hideActivityIndicator()
                return
        }

        SharedInformation.instance().serachBarText = searchBar.text

        _ = GetRepositoriesServiceWebAPI.instance().getRepositoriesServiceDetails() { (response) in
            DispatchQueue.main.async {
                guard response == nil else {
                    // step4: do action (like display data on UI, or go to different screen..etc)
                    print(response!)
                    self.tenRowsArray.removeAll()
                    self.repositoriesItemsArray.removeAll()
                    self.repoArray.removeAll()
                    self.filteredDateSlectedData.removeAll()
                    
                    SharedInformation.instance().repositoriesResponse = response
                    print(SharedInformation.instance().repositoriesResponse?.items?.count as Any)
                    self.repositoriesItemsArray = (SharedInformation.instance().repositoriesResponse?.items)!
                    self.tenRowsArray = Array(self.repositoriesItemsArray.prefix(10))

                    if self.checkDate == true {
                        for filterDate in self.tenRowsArray {
                            if self.selectedDateString == filterDate.name {
                                self.filteredDateSlectedData.append(filterDate)
                            }
                        }
                        if self.filteredDateSlectedData.count == 0 {
                            self.checkDate = false
                        }
                        
                        self.hideActivityIndicator()
                    } else {
                        for watcherCount in self.tenRowsArray {
                            let des = self.tenRowsArray.sorted { (a, b) -> Bool in
                                print("a = \(String(describing: a.watchers_count))")
                                print("b = \(String(describing: b.watchers_count))")
                                guard let heightA = a.watchers_count, let heightB = b.watchers_count else {
                                return false
                                }
                                return heightA > heightB
                            }
                            self.repoArray = des
                        }
                        self.hideActivityIndicator()
                    }
                    self.hideActivityIndicator()
                    self.tableview.reloadData()
                    return
                }
            }
        }
    }
}

//MARK:- UITableView
extension ViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if checkDate == true {
            let numberOfRows = filteredDateSlectedData.count
            return numberOfRows
        }else {
            let numberOfRows = repoArray.count
            return numberOfRows
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as? TableViewCell
        if checkDate == true {
            let data = filteredDateSlectedData[indexPath.row]
            cell?.nameLabel.text = data.name
            cell?.fullNameLabel.text = data.full_name
            cell?.watcherCountLabel.text = data.watchers_count?.description
            cell?.avatarImage?.image = defalutImage
            cell?.uiView?.clipsToBounds = true
            cell?.uiView?.layer.cornerRadius = 10
            cell?.uiView?.layer.borderColor = UIColor.gray.cgColor
            return cell!
        } else{
            let data = repoArray[indexPath.row]
            cell?.nameLabel.text = data.name
            cell?.fullNameLabel.text = data.full_name
            cell?.watcherCountLabel.text = data.watchers_count?.description
            cell?.avatarImage?.image = defalutImage
            cell?.uiView?.clipsToBounds = true
            cell?.uiView?.layer.cornerRadius = 10
            cell?.uiView?.layer.borderColor = UIColor.gray.cgColor
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let avatarImageUrl = checkDate ? filteredDateSlectedData[indexPath.row].owner?.avatar_url : repoArray[indexPath.row].owner?.avatar_url
        if avatarImageUrl?.count == nil {
            (cell as? TableViewCell)?.avatarImage.image = defalutImage
            print(indexPath.row)
            return
        } else {
            print(avatarImageUrl as Any)
            // Checking Cache
            if let dict = UserDefaults.standard.object(forKey: "ImageCache") as? [String:String]{
                if let path = dict[(avatarImageUrl! as NSString) as String] {
                    if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                        let img = UIImage(data: data)
                        // If cache is there, Loading into cell from Cache
                        (cell as? TableViewCell)?.avatarImage.image = img
                        return
                    }
                }
            }
            //lazy loading
            let session = URLSession.shared
            let imageURL = URL(string: avatarImageUrl!)
            let task = session.dataTask(with: imageURL!) { (data, response, error) in
                guard error == nil else {
                    return
                }
                DispatchQueue.main.async {
                    NSLog("cell number \(indexPath.row)")
                    if let image = UIImage(data: data!) {
                        // calling from API
                        (cell as? TableViewCell)?.avatarImage.image = image
                        // StoringImages into Cache
                        StorageImageViewController.storeImage(urlstring: (avatarImageUrl! as NSString) as String, img: image)
                    }
                }
            }
            task.resume()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! TableViewCell

        if checkDate == true {
            if tableView.isHidden == true {
                APIInterface.instance().showAlert(title: "Oops", message: "Didn't find any  TableView data")
            } else {
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ContributorsDetailsViewController") as? ContributorsDetailsViewController
                vc!.repoDetails = filteredDateSlectedData[indexPath.row]
                vc?.avatarImage = cell.avatarImage.image
                navigationController?.pushViewController(vc!, animated: true)
            }

        } else {
            if tableView.isHidden == true {
                APIInterface.instance().showAlert(title: "Oops", message: "Didn't find any  TableView data")
            } else {
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ContributorsDetailsViewController") as? ContributorsDetailsViewController
                vc!.repoDetails = repoArray[indexPath.row]
                vc?.avatarImage = cell.avatarImage.image
                navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
}

//MARK:- SearchBar
extension ViewController : UISearchBarDelegate, UISearchResultsUpdating {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        //do something
        if searchBar.text?.count == 0 {
            searchBar.resignFirstResponder() //hide keyboard
            tableview.isHidden = true
        }else {

            searchBar.resignFirstResponder()
            isSeacrhActive = false
            getRepositoriesServiccall()
            checkDate = false
            tableview.isHidden = false
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        isSeacrhActive = false
        tableview.isHidden = true
        checkDate = false
        tableview!.reloadData()
    }
}

//MARK:- Animations
extension ViewController {
    
    func navigationController(
        _ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationController.Operation,
        from fromVC: UIViewController,
        to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        transitionViewController.popStyle = (operation == .pop)
        return transitionViewController
    }
}

//MARK:- FilterDateDelegate
extension ViewController : FilterDateDelegate {
    
    func textFiledValue(value: String) {
        
        if value != "" {
            checkDate = true
            self.selectedDateString = value
        } else {
            checkDate = false
            self.selectedDateString = value
        }
    }
}

extension ViewController {
    
    func showActivityIndicator() {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
}



