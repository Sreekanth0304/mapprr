//
//  ContributorsRepoListDetailsViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit

class ContributorsRepoListDetailsViewController: UIViewController {

    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var tableview: UITableView!

    var contributorsDetails : ContributorsModels? = nil
    var avatarImage : UIImage?
    var contributorsItemsArray = [ContributorsRepoListModels]()
    var defalutImage = UIImage(named: "Empty-Image.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SharedInformation.instance().contributorsRepoListUrl = (contributorsDetails?.repos_url)!
        avatarImageView.image = avatarImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if SharedInformation.instance().contributorsRepoListUrl == "" {
            
            APIInterface.instance().showAlert(title: "", message: "This contributor does not has repo list")
        } else {
            getContributorsRepoListServiccall()
        }
        
        tableview.separatorStyle = .none
        tableview.estimatedRowHeight = 80
        tableview.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITableView
extension ContributorsRepoListDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return contributorsItemsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ContributorsRepoListTableViewCell") as? ContributorsRepoListTableViewCell
        let data = contributorsItemsArray[indexPath.row]
        cell?.titleLabel.text = data.name  ?? "Nil"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "RepoDetailsViewController") as? RepoDetailsViewController
        vc?.contributorsRepoListDetails = contributorsItemsArray[indexPath.row]
        navigationController?.pushViewController(vc!, animated: true)
    }
}

//MARK:- Get data from API
extension ContributorsRepoListDetailsViewController {
    
    func getContributorsRepoListServiccall() {
        
        _ = GetContributorsRepoListServiceWebAPI.instance().getContributorsRepoListServiceDetails() { (response) in
            DispatchQueue.main.async {
                guard response == nil else {
                    // step4: do action (like display data on UI, or go to different screen..etc)
                    print(response!)
                    self.contributorsItemsArray.removeAll()
                    SharedInformation.instance().contributorsRepoListResponse = response
                    self.contributorsItemsArray = SharedInformation.instance().contributorsRepoListResponse!
                    
                    if self.contributorsItemsArray.count == 0 {
                        APIInterface.instance().showAlert(title: "", message: "This contributor does not has repo list")
                    }
                    self.tableview.reloadData()
                    return
                }
            }
        }
    }
}
