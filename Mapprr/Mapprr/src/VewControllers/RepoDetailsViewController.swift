//
//  RepoDetailsViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 24/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit

class RepoDetailsViewController: UIViewController {

    var contributorsRepoListDetails : ContributorsRepoListModels? = nil
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var projectLinkButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!

    var defalutImage = UIImage(named: "Empty-Image.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        avatarImageView.image = defalutImage
        nameLabel.text = contributorsRepoListDetails?.name ?? "Nil"
        projectLinkButton.setTitle(contributorsRepoListDetails?.html_url, for: .normal)
        descriptionTextView.text = contributorsRepoListDetails?.description ?? "Nil"
        downLaodImage()
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    func downLaodImage() {
        
        let avatarImageUrl = contributorsRepoListDetails?.owner?.avatar_url
        if avatarImageUrl?.count == nil {
            avatarImageView.image = defalutImage
            return
        } else {
            //lazy loading
            let session = URLSession.shared
            let imageURL = URL(string: avatarImageUrl!)
            let task = session.dataTask(with: imageURL!) { (data, response, error) in
                guard error == nil else {
                    return
                }
                DispatchQueue.main.async {
                    if let image = UIImage(data: data!) {
                        // calling from API
                        self.avatarImageView.image = image
                    }
                }
            }
            task.resume()
            
        }
    }
}
