//
//  ContributorsDetailsViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 24/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit

class ContributorsDetailsViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var projectLinkButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var contributorsCollectionView: UICollectionView!
    
    let imagePicker = UIImagePickerController()
    var selectedImageOrDocumentURLString = String()
    var selectedImageOrDocumentURLData = Data()
    var selectedImage = UIImageView()
    
    var repoDetails : RepositoriesItems? = nil
    var avatarImage : UIImage?
    var contributorsItemsArray = [ContributorsModels]()
    var defalutImage = UIImage(named: "Empty-Image.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SharedInformation.instance().contributorsUrl = (repoDetails?.contributors_url)!
        avatarImageView.image = avatarImage
        nameLabel.text = repoDetails?.name
        projectLinkButton.setTitle(repoDetails?.html_url, for: .normal)
        descriptionTextView.text = repoDetails?.description
        
        contributorsCollectionView.isHidden = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getContributorsServiccall()
    }
    
    
    @IBAction func projectLinkButtonTapped(_ sender: Any) {
        

        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        vc?.repoDetails = repoDetails
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
}

//MARK:- SaveCoreData
extension ContributorsDetailsViewController {

    func showAlert(title: String, message: String) {
        
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message:
                message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

//MARK:- UITextViewDeleate
extension ContributorsDetailsViewController: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

//MARK:- Get data from API
extension ContributorsDetailsViewController {
    
    func getContributorsServiccall() {
        
        _ = GetContributorsServiceWebAPI.instance().getContributorsServiceDetails() { (response) in
            DispatchQueue.main.async {
                guard response == nil else {
                    // step4: do action (like display data on UI, or go to different screen..etc)
                    print(response!)
                    self.contributorsItemsArray.removeAll()
                    SharedInformation.instance().contributorsResponse = response
                    self.contributorsItemsArray = SharedInformation.instance().contributorsResponse!
                    self.contributorsCollectionView.reloadData()
                    return
                }
            }
        }
    }
}


//MARK:- UICollectionView
extension ContributorsDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 71.0, height: 71.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return contributorsItemsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContributorsDetailsCollectionViewCell", for: indexPath as IndexPath) as! ContributorsDetailsCollectionViewCell
        myCell.contentView.backgroundColor = .yellow
        let data = contributorsItemsArray[indexPath.row]
        myCell.nameLabel.text = data.login
        myCell.nameLabel.textColor = .blue
        myCell.avatarImageView.image = defalutImage
        return myCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        let avatarImageUrl = contributorsItemsArray[indexPath.row].avatar_url
        if avatarImageUrl?.count == nil {
            (cell as? ContributorsDetailsCollectionViewCell)?.avatarImageView.image = defalutImage
            print(indexPath.row)
            return
        } else {
            print(avatarImageUrl as Any)
            // Checking Cache
            if let dict = UserDefaults.standard.object(forKey: "ImageCache") as? [String:String]{
                if let path = dict[(avatarImageUrl! as NSString) as String] {
                    if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                        let img = UIImage(data: data)
                        // If cache is there, Loading into cell from Cache
                        (cell as? ContributorsDetailsCollectionViewCell)?.avatarImageView.image = img
                        return
                    }
                }
            }
            //lazy loading
            let session = URLSession.shared
            let imageURL = URL(string: avatarImageUrl!)
            let task = session.dataTask(with: imageURL!) { (data, response, error) in
                guard error == nil else {
                    return
                }
                DispatchQueue.main.async {
                    if let image = UIImage(data: data!) {
                        // calling from API
                        (cell as? ContributorsDetailsCollectionViewCell)?.avatarImageView.image = image
                        // StoringImages into Cache
                        ContributorsStorageImageViewController.storeImage(urlstring: (avatarImageUrl! as NSString) as String, img: image)
                    }
                }
            }
            task.resume()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ContributorsDetailsCollectionViewCell
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ContributorsRepoListDetailsViewController") as? ContributorsRepoListDetailsViewController
        vc?.avatarImage = cell.avatarImageView.image
        vc?.contributorsDetails = contributorsItemsArray[indexPath.row]
        navigationController?.pushViewController(vc!, animated: true)
    }
}
