//
//  WebViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 24/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    var repoDetails : RepositoriesItems? = nil
    var webView: WKWebView!
    
    @IBOutlet weak var webUIView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func loadView() {
        super.loadView()
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        webView = WKWebView()
        webView.navigationDelegate = self
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.webUIView.frame.size.width, height: self.webUIView.frame.size.height))
        self.webUIView.addSubview(webView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL(string: (repoDetails?.html_url)!)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    @IBAction func bakcButtonTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
