//
//  FilterViewController.swift
//  Mapprr
//
//  Created by DWC-LAP-539 on 22/02/20.
//  Copyright © 2020 DWC-LAP-539. All rights reserved.
//

import UIKit

@objc protocol FilterDateDelegate : NSObjectProtocol {
    
    func textFiledValue(value: String)
}

class FilterViewController: UIViewController {

    @IBOutlet weak var clealAllButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var imageChange: UIImageView!
    
    var isClealAllSelected = false
    weak var filterDateDelegate: FilterDateDelegate?
    let dateFormatter = DateFormatter()
    var viewController : ViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func clearAllButtonTapped(_ sender: Any) {

        if imageChange.image == UIImage(named: "Check-mark.png") {
            
            isClealAllSelected = !isClealAllSelected
            if !isClealAllSelected  {
                nameTextField.text = ""
                clealAllButton.setTitleColor(.darkGray, for: .normal)
                imageChange.image = nil
            } else {
                clealAllButton.setTitleColor(.systemBlue, for: .normal)
            }
        }
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        
        if nameTextField.text == "" {
            self.filterDateDelegate?.textFiledValue(value: "")
            navigationController?.popViewController(animated: true)
        } else {

            DispatchQueue.main.async {
                
                self.getTextFiledValue()
                let alert = UIAlertController(title: "Are you sure!", message: "You Want to add this filter \(self.nameTextField.text?.description ?? "") date?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.viewController?.searchBar.text = SharedInformation.instance().serachBarText
                    self.viewController!.getRepositoriesServiccall()
                    self.navigationController?.popViewController(animated: true)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
                    self.imageChange.image = nil
                }))
                self.present(alert, animated: true)
            }
        }
    }
}

extension FilterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        nameTextField.resignFirstResponder()
        return true
    }
}

extension FilterViewController {
    
    func getTextFiledValue() {
        
        self.filterDateDelegate?.textFiledValue(value: nameTextField.text!)
        imageChange.image = UIImage(named: "Check-mark.png")
        isClealAllSelected = !isClealAllSelected
        if !isClealAllSelected  {
            
            clealAllButton.setTitleColor(.darkGray, for: .normal)
        } else {
            clealAllButton.setTitleColor(.systemBlue, for: .normal)
        }
    }
}


